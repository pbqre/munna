#!/usr/bin/env python
"""
Name: Munna, Another Discord bot after `Kaleen` for our discord server.
Author: Vivek Kumar
"""
import os
import sys
from getpass import getpass
from base.config import BOT_PREFIX, CLIENT_SECRET, USER_GREETING_MESSAGE
from base import database
from base import operations
from base.validators import is_email, is_password_valid
from base.mailer import send_confirmation_mail
from discord import Client


# Bot Section...

client = Client()

@client.event
async def on_member_join(member):
    print(USER_GREETING_MESSAGE.substitute())
    


@client.event
async def on_message(message):
    print("HHHH")
    data = str(message.content).lower().split()
    print(data)
    if message.author == client.user:
        return None
    if len(data) > 2:
        print("!")
        if data[0] == "!munna":
            print("2")
            if data[1] == "join":
                if is_email(data[2]):
                    # Save the user to the database.
                    operations.create_mail(data[2])
                    send_confirmation_mail(data[2])
                    await message.channel.send(f"Congratulation @{message.author}, You are now part of something big. :smile:")
                    await message.channel.send(f"@{message.author}, I have also sent a mail for you :smile:, see it.")
    
                else:
                    await message.channel.send(f"Ahhh! @{message.author}, Email is not valid.")






# Script Section ...
if __name__ == "__main__":
    # Command: ./main.py -h/--help
    client.run(CLIENT_SECRET)
    if len(sys.argv) > 1:
        if sys.argv[1] == '-h' or sys.argv[1] == '--help':
            print(config.HELP_TEXT)

    # Command: ./main.py create tables
    elif len(sys.argv) > 2:
        if sys.argv[1] == "create":
            db = database.db
            db.connect()

            if sys.argv[2] == "tables":
                db.create_tables([database.Mail, database.User])
                print(f"{config.PREFIX}Tables Mail and User are Created.")

            elif sys.argv[2] == "user":
                email = input(f"{config.PREFIX} Enter email: ")
                password = getpass(f"{config.PREFIX} Enter password: ")
                result = operations.create_user(email, password)
                print(f"{config.PREFIX} {result[1]}")

            elif sys.argv[3] == "mail":
                email = input(f"{config.PREFIX} Enter mail: ")
                result = operations.create_mail(email)
                print(f"{config.PREFIX} {result[1]}")

                




