# Munna


## How to setup project
- Get the repository.
	1. `git clone https://gitlab.com/pbqre/munna`
	2. `cd Munna`
- Create virtual enviroment.
	1. `python3 -m venv env`
	2. `. ./env/bin/activate`
	3. `pip install -r ./requirements.txt`
- Create the secret file.
	1. `touch .env`
	2. Add the below content.
```bash
export CLIENT_SECRET="discord-bot-client-secret"
export EMAIL="email-you wanna-user"
export PASSWORD="email-password"
```
- To source the .env in each activation of enviroment.
	1. `echo 'set -a; source .env; set +a' >> ./env/bin/activate`
	2. Activate the enviroment again.
	3. All Set.

