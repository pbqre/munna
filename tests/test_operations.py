import unittest
import peewee
import hashlib
from base import config
from base import tester
from base import database
from base import operations

class TestOperations(unittest.TestCase):
    def setUp(self):
        self.test_db = peewee.SqliteDatabase(":memory:")
        self.test_db.bind(tester.get_models(), bind_refs=False, bind_backrefs=False)
        self.test_db.connect()
        self.test_db.create_tables(tester.get_models())
        self.test_user_data = {
            'email': 'vivekascoder@gmail.com',
            'email_1': 'vivek',
            'password': '1223424',
            'password_1': 'ad3r3'
        }

    def test_create_user(self):
        result = operations.create_user(email=self.test_user_data['email'], password=self.test_user_data['password'])
        self.assertEqual(result[0], True)
    
    def test_hashed_password_function(self):
        hash = hashlib.sha256()
        hash.update(self.test_user_data['password_1'].encode())
        self.assertEqual(hash.hexdigest(), operations.hash_password(self.test_user_data['password_1']))

    def test_create_user_wrong_mail(self):
        result = operations.create_user(email=self.test_user_data['email_1'], password=self.test_user_data['password'])
        self.assertEqual(result[0], False)

    def test_check_user_created(self):
        user = database.User.get(database.User.email == self.test_user_data['email'])
        print(user)

    def tearDown(self):
        self.test_db.drop_tables(tester.get_models())
        self.test_db.close()
    

if __name__ == "__main__":
    unittest.main()