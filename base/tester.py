import peewee
import os
from base import config
from base import database


def get_models():
    """
    Return: list of database tables.
    """
    return [database.Mail, database.User]

