#!/usr/bin/env python
import os
import smtplib
import ssl
import sys
from base.config import CONFIRMATION_MAIL
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def send_text_mail(reciever, subject, body):
    """
    reciever: Email of the reciever.
    subject: Subject of the mail.
    body: body of the mail.
    """
    
    message = MIMEMultipart("alternative")
    message["Subject"] = subject
    message["From"] = os.getenv("EMAIL")
    message["To"] = reciever
    part = MIMEText(body, 'plain')
    message.attach(part)
    context = ssl.create_default_context()

    with smtplib.SMTP_SSL("smtp.gmail.com", os.getenv("SMTP_PORT")) as server:
        server.login(os.getenv("EMAIL"), os.getenv("PASSWORD"))
        server.sendmail(os.getenv("EMAIL"), reciever, message.as_string())


def send_confirmation_mail(reciever):
    """
    A basic mail will be sent to the user account when the user joins.
    """
    send_text_mail(reciever, "Thanks for joining Divcorn.", CONFIRMATION_MAIL)



# In case if you wanna use this script only:
if __name__ == "__main__":
    if len(sys.argv) > 4:
        print("USE: ./mailer.py <reciever@gmail.com> 'Subject Goes Here' 'Then body'")
        exit(1)
    else:
        reciever = sys.argv[1]
        subject = sys.argv[2]
        body = sys.argv[3]
        send_text_mail(reciever, subject, body)
        print(f"[MAILER]: Done sent to {reciever}")
        exit(0)
