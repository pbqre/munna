#!/usr/bin/env python
import os
import pathlib
from string import Template

# For base modules.
BASE_DIR = pathlib.Path.cwd()
DATABASE_PATH = str((BASE_DIR / 'db.sqlite3'))
SMTP_PORT = 463
EMAIL_RE = r"^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$"

# Regarding Printing
HELP_TEXT = """
 __  __
 |  \/  |_   _ _ __  _ __   __ _ 
 | |\/| | | | | '_ \| '_ \ / _` |
 | |  | | |_| | | | | | | | (_| |
 |_|  |_|\__,_|_| |_|_| |_|\__,_|
==================================
>Managing Mails With Discord Bot!! 

💡 How To Use:
    1. `-h`/`--help`: Show you this string.
    2. `create table`: Will Recreate the database table.
    3. `create user`: Will create a new user.
    4. `add <email>`: Will add a new email to mailing list/

🛠️ :: Constructed by `Vivek Kumar`.
"""
PREFIX = "[MUNNA]::"

# Mail Templates
CONFIRMATION_MAIL = """
Hello, I am Munna, Mail Bot at Divcorn Server.

Thanks for joining our mailing list now you wont be able to miss any notification from us.

- @Munna
"""

# Bot configuration
BOT_PREFIX = ("!", "?")
CLIENT_SECRET = os.getenv("CLIENT_SECRET")

USER_GREETING_MESSAGE = Template("""
Hello, $user I am Munna, discord bot of Divcorn server.
If you are having a mindset of not missig any notification 
from our community then please join our mailing list, by following command.
::
`!munna join <your-email-address>`
""")