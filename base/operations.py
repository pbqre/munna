#!/usr/bin/env python

import hashlib
from base.database import Mail, User
from base import validators
import peewee


def hash_password(password):
   """
   Description: Return the hexify sha256 cipher.
   """
   hash = hashlib.sha256()
   hash.update(password.encode())
   return hash.hexdigest()

def create_mail(mail):
    """
    Add the given mail to the database after validating.
    """
    if validators.is_email(mail):
        mail = Mail(email=mail)
        mail.save()
        return True, "Mail is added to Mailing List."
    return False, "Email is not valid"
  
def create_user(email, password):
    """
    Save the giver username and password to the database.
    """
    if validators.is_email(email) == True:
        if validators.is_password_valid(password)[0]:
            hashed_password = hash_password(password)
            user = User(email=email, password=hashed_password)
            user.save()
            return True, user

        return False, "Password is not valid."
    return False, "Email is not  valid."


def check_user(email, password):
    hashed_password = hash_password(password)
    user = User.select().where(User.email==email, User.password==hashed_password)
    if user.exists():
        return True
    return False
    
