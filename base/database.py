#!/usr/bin/env python
import peewee 
from . import config
import re

db = peewee.SqliteDatabase(config.DATABASE_PATH)

class User(peewee.Model):
    email = peewee.CharField(unique=True)
    password = peewee.CharField()

    class Meta:
        database = db

class Mail(peewee.Model):
    email = peewee.CharField(unique=True)
    status = peewee.BooleanField(default=True)

    class Meta:
        database = db

